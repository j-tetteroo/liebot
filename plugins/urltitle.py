from builtins import object
import time
import unittest
import re
import extraction, requests

from util import hook, timesince

@hook.singlethread
@hook.event('PRIVMSG', ignorebots=False)
def urltitle(paraml, input=None, db=None, bot=None, say=None):
  sUrl = re.search( r'(https?|ftp)://(-\.)?([^\s/?\.#-]+\.?)+(/[^\s]*)?', paraml[1], re.M|re.I)
  if sUrl:
    url=sUrl.group(0)
    headers = {'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36'} 
    r = requests.get(url, headers=headers)
    if (r.encoding != None):
        html=r.text 
        extracted=extraction.Extractor().extract(html, source_url=url)
        say(f"Title: {extracted.title}")
