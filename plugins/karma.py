from builtins import object
import time
import unittest

from util import hook, timesince


def db_init(db):
    "check to see that our db has the the seen table and return a connection."
    db.execute("CREATE TABLE IF NOT EXISTS karma(phrase, score, primary key(phrase))")
    db.commit()

def get_karma_score(phrase, db=None):
    db_init(db)
    score = db.execute("SELECT score FROM karma WHERE phrase=?",(phrase,)).fetchone()
    db.commit()
    if score is None:
        return 0
    else:
        return score[0]

def set_karma(phrase, positive, db=None):
    db_init(db)
    score=get_karma_score(phrase, db=db)
    if positive:
        score+=1
    else:
        score-=1
    db.execute("insert or replace into karma(phrase, score) values(?, ?)", (phrase,(score)))
    db.commit()

    return score

def get_top5(direction, db=None, say=None):
    db_init(db)
    karma = db.execute(f"SELECT phrase, score FROM karma ORDER BY score {direction} LIMIT 5")
    db.commit()
    i=1
    for row in karma.fetchall():
        phrase=row[0]
        score=row[1]
        say(f"{i}) {phrase} is {score}")
        i=i+1

@hook.singlethread
@hook.event('PRIVMSG', ignorebots=False)
def karma(paraml, input=None, db=None, bot=None, say=None):
    phrase=paraml[1][:-2]
    signs=paraml[1][-2:]
    if str(phrase).lower().endswith(" c") or str(phrase).lower() == "c": # REQUEST BY INZ
      return
    if signs == "++":
       score=set_karma(phrase, True, db)
       say(f"{phrase} is now {score}")
    if signs== "--":
       score=set_karma(phrase, False, db)
       say(f"{phrase} is now {score}")

@hook.command('good', autohelp=False)
@hook.command(autohelp=False)
def top(inp, nick='', chan='', db=None, input=None, say=None):
    ".top -- Show the top 5 karma"
    get_top5("DESC", db, say)

@hook.command('bad', autohelp=False)
@hook.command(autohelp=False)
def bottom(inp, nick='', chan='', db=None, input=None, say=None):
    ".bottom -- Show the top 5 karma"
    get_top5("ASC", db, say)
